(function () {
    'use strict';

    angular
        .module('angularStructure.example2')
        .factory('example2Service', example2Service);

    /* @ngInject */
    function example2Service($log, $q, Restangular) {
        // service object
        var service = {
            getTags: getTags,
            getTag: getTag,
            testService: testService
        };

        var tags = null;

        return service;

        /////////////////// Public Functions ////////////////////

        function getTags() {
            var defer = $q.defer();

            if (tags) {
                defer.resolve(tags);
            } else {
                Restangular.all('tags').getList().then(function (response) {
                    tags = response;
                    defer.resolve(response);
                }, function (errorResponse) {
                    defer.reject(errorResponse);
                });
            }

            return defer.promise;
        }

        function getTag(tagId) {
            return Restangular.one('tags', tagId).get();
        }

        function testService() {
            $log.info('You call a function from service.');
            privateFunction();
        }

        /////////////////// Private Functions ////////////////////

        function privateFunction() {
            $log.info('A private function is called in service. Check out example2.service.js');
        }

    }

})();