(function () {
    'use strict';

    angular
        .module('angularStructure.example2')
        .controller('Example2TabsController', Example2TabsController);

    /* @ngInject */
    function Example2TabsController($log, $rootScope, tags, example2Service) {
        // set view model
        var vm = this;

        // set view model variables
        vm.tags = tags;

        // set view model variables
        vm.test = test;
        vm.triggerEvent = triggerEvent;

        // initiliaze controller
        activate();

        //////////////////// Public Functions ////////////////////

        function activate() {
            $log.info('Example 2 tabs controller is initialized. This module has sub folder so checkout its structure');

            /*vm.tags = example2Service.getTags().$object;*/

            /* example2Service.getTags().then(function (tags) {
                 $log.info('Tags are resolved. Here is the response', tags);
                 vm.tags = tags;
             }, function (error) {
                 $log.warn('Somethings gone wrong on backend. Here is the error', error);
             });*/


            // restangular returns u not a simple list but an object decorated with restangular functions
            // so if you want to reach other url related with your current object you use this object
            // as an example if you want to get specific tag all you have to do is just call get method with id
            // vm.tags.get(tagId).then...
            // 
            // vm.tag.remove()
        }

        function test(label) {
            alert('Button at tab ' + label + ' is clicked...');
            example2Service.testService();
        }

        function triggerEvent() {
            $rootScope.$broadcast('remy-event', {
                name: 'Remy',
                mentor: 'Poyraz'
            });
        }

    }

}());