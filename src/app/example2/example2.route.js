(function () {
    'use strict';

    angular
        .module('angularStructure.example2')
        .config(example2Routes);

    /* @ngInject */
    function example2Routes($stateProvider) {
        $stateProvider
            .state('app.example2', {
                url: '/example2',
                controller: 'Example2TabsController as vm',
                templateUrl: 'app/example2/tabs/example2-tabs.html',
                resolve: {
                    tags: getTags
                }
            })
            .state('app.example2.child', {
                url: '/child',
                controller: 'Example2TabsController as vm',
                templateUrl: 'app/example2/tabs/example2-tabs.html',
                resolve: {
                    tabs: function (tabs) {
                        return tabs;
                    }
                }
            });
    }

    /* @ngInject */
    function getTags($log, example2Service) {
        /*$log.info('Example 2 tabs resolve on state change checkout example2.routes.js');
        return ['one', 'two', 'three', 'four'];*/
        return example2Service.getTags().then(function (tags) {
            $log.info('Tags are resolved. Here is the response', tags);
            return tags;
        });
    }

})();