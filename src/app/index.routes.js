(function () {
    'use strict';

    angular
        .module('angularStructure')
        .config(indexRoutes);

    /* @ngInject */
    function indexRoutes($stateProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: 'app/index.html'
            });
    }

})();