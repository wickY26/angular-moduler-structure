(function () {
	'use strict';

	angular
		.module('angularStructure', [
			// third party modules
			'ngAnimate',
			'ngMaterial',
			'restangular',
			'ui.router',
			// sub modules
			'angularStructure.example',
			'angularStructure.example2'
		]);

})();