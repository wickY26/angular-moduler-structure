(function () {
    'use strict';

    angular.module('angularStructure')
        .directive('remyHeader', remyHeader);

    function remyHeader() {
        // Runs during compile
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/header/header.html',
            scope: {},
            controller: HeaderController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /////////////////// Directive Controller ////////////////////

        /* @ngInject */
        function HeaderController($rootScope) {
            // set view model context
            var vm = this;

            // set view model variables
            vm.title = 'Remy Header';

            // set view model functions
            vm.test = test;

            // initialize controller
            activate();

            //////////////////// View Model Functions ////////////////////

            function activate() {
                console.log('Header Directive is started');
            }

            function test() {
                console.log('test function is called from header');
            }

            //////////////////// Event Listeners ////////////////////

            $rootScope.$on('remy-event', function (event, parameter) {
                console.log('This event triggered.',event);
                console.log('Header directive catch it with this parameter.', parameter);
            });

        }

    }

})();