(function () {
    'use strict';

    angular.module('angularStructure')
        .config(coreConfigs)
        .run(coreRunConfigs);

    /* @ngInject */
    function coreConfigs($urlRouterProvider, RestangularProvider) {
        RestangularProvider.setBaseUrl('http://private-04234-perzzleapiv1.apiary-mock.com/v1');
        // redirect to example view when route not found
        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            $state.go('app.example');
        });
    }

    /* @ngInject */
    function coreRunConfigs($log, $rootScope) {
        // listen state changes error
        $rootScope.$on('$destroy', $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            $log.warn(error);
        }));
    }

})();