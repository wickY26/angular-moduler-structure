(function () {
    'use strict';

    angular
        .module('angularStructure.example')
        .config(exampleRoutes);

    /* @ngInject */
    function exampleRoutes($stateProvider) {
        $stateProvider
            .state('app.example', {
                url: '/example',
                controller: 'ExampleController as vm',
                templateUrl: 'app/example/example.html'
            });
    }

})();