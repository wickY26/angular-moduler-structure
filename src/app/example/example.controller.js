(function () {
    'use strict';

    angular
        .module('angularStructure.example')
        .controller('ExampleController', ExampleController);

    /* @ngInject */
    function ExampleController($log) {
        // set view model
        var vm = this;

        // set view model variables
        vm.tabs = ['one', 'two', 'three', 'four'];

        // set view model variables
        vm.test = test;

        // initiliaze controller
        activate();

        //////////////////// Public Functions ////////////////////

        function activate() {
            $log.info('Example controller is initialized. You can use this activate block to initialize variables or make some changes after page loads...');
        }

        function test(label) {
            alert('Button at tab ' + label + ' is clicked...');
        }

    }

}());